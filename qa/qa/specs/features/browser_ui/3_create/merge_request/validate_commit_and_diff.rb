module QA
  context 'Create' do
    describe 'Validating the commit and commit diffs in the merge request tab' do
      before do
          Runtime::Browser.visit(:gitlab, Page::Main::Login)
          Page::Main::Login.perform(&:sign_in_using_credentials)

          @project = Resource::Project.fabricate_via_api! do |project|
            project.name = 'project'
        end

        @merge_request_title = 'Merge request title'
        @merge_request_description = 'Merge request description'
      end

      it 'validates the commits tab for the commits' do
        Resource::MergeRequest.fabricate_via_browser_ui! do |merge_request|
              merge_request.project = @project
              merge_request.title = @merge_request_title
              merge_request.description = @merge_request_description
            end

            Page::MergeRequest::Show.perform do |merge_request|
              merge_request.click_commits_tab
              expect(merge_request).to have_content('This is a test commit')
          end
      end

       it 'validates the changes tab for the commit diff' do
        Resource::MergeRequest.fabricate_via_browser_ui! do |merge_request|
              merge_request.project = @project
              merge_request.title = @merge_request_title
              merge_request.description = @merge_request_description
            end

            Page::MergeRequest::Show.perform do |merge_request|
              merge_request.click_diffs_tab
              expect(merge_request).to have_content('File Added')
            end
          end
        end
      end
    end
